import { useState } from 'react';
import './App.css';
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

const initialTasks = [
  {id: "1", text: "React.js"},
  {id: "2", text: "HTML/CSS"},
  {id: "3", text: "AWS"},
  {id: "4", text: "JavaScript"},
] 

function App() {
  const [tasks, setTasks] = useState(initialTasks)

  const onDragEnd = () => {

  }
  
  return (
    <DragDropContext onDragEnd={ (result) => console.log(` erereF result`)}>
      <div className="app">
      <h1>Estudiar</h1>
      <Droppable droppableId='tasks'>
        { (droppableProvided) => (
          <ul 
          {...droppableProvided.droppableProps}
          ref={droppableProvided.innerRef}
          className='task-container'>
        {
          tasks.map( (task, index) => (
            <Draggable  draggableId={task.id} key={task.id} index={index}>
            {
              (draggableProvided) => (
                <li 
                {...draggableProvided.draggableProps}
                ref={draggableProvided.innerRef}
                {...draggableProvided.dragHandleProps}
                className='task-item'>
            {task.text}
            </li>
              )
            }
            </Draggable>
          )
          )
        }
        {droppableProvided.placeholder}
      </ul>
        )}
      </Droppable>
    </div>
    </DragDropContext>
    
  );
}

export default App;
